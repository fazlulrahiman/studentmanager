import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:level/dashboard.dart';
import 'package:level/studentdetails.dart';
import 'package:level/studentprofile.dart';
import 'package:level/subjectprofile.dart';

void main() {
  runApp( MyApp());
}

class MyApp extends StatelessWidget {


  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
    home: Dashboard(),builder: EasyLoading.init(),
    );
  }
}
