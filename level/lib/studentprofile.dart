import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/http.dart' as http;
import 'package:level/ModelClass/ClassRoomModel.dart';

import 'Utils/Urls.dart';

class StudentProfile extends StatefulWidget{

  String id = "";
  StudentProfile(this.id);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return StudentState(this.id);
  }

}
  class StudentState extends State<StudentProfile>{
    List<ClassRoomModel>classlist=[];

    String Selected_class_id="",Selected_sub_id="";
    String class_id="",sub_id="";
    String id = "";
    String name="";
    String age="";
    String email="";
    StudentState(this.id);
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    GetStudent();
    GetClass();
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: SafeArea(
        child:Column(

        children:[
          Expanded(flex: 0,
            child:Container(height:60,color: Colors.blueGrey,
              child: Row(
                children: [
                  Padding(padding: EdgeInsets.only(left: 5),
                    child:GestureDetector(
                      onTap: (){
                        Navigator.pop(context, true);

                      },
                    child: Icon(Icons.arrow_back,color: Colors.white,),
                  )
                  ),
                  Padding(padding: EdgeInsets.only(left: 5),
                    child: Text("Student Profile",style: TextStyle(fontSize:18,fontWeight: FontWeight.bold,color: Colors.white)),

                  )
                ],
              ),

            )
        ),
        Expanded(flex: 1,
        child: Column(mainAxisAlignment: MainAxisAlignment.center,mainAxisSize: MainAxisSize.max,
          children: [


            Expanded(flex:0,

            child: Padding(padding: EdgeInsets.only(top: 30),
              child:Center(
              child: Image.asset("assets/images/pf.logo.png",height:100,),
            )
            ),
            ),
            Expanded(flex: 0,
            child: Column(mainAxisSize: MainAxisSize.max,mainAxisAlignment: MainAxisAlignment.center,
              children: [
                 Padding(padding: EdgeInsets.only(top: 20),
                  child: Text(name,style: TextStyle(fontSize:25,fontWeight: FontWeight.bold,color: Colors.black)),
                ),
                Padding(padding: EdgeInsets.only(),
                child: Text("Age: "+age,style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,color: Colors.black)),
                ),
                Padding(padding: EdgeInsets.only(),
                child: Text("Email: "+email,style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,color: Colors.black)),
                )
              ],
            ),

            ),
            Expanded(flex: 0,
            child: Padding(padding: EdgeInsets.only(top: 50),
            child:GestureDetector(
              onTap: (){
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text('Class List'),
                        content: setupAlertDiaload(),
                      );
                    });

              },
            child: Container(height:50,width:150,decoration: BoxDecoration(borderRadius: BorderRadius.circular(20),color: Colors.blueGrey),
              child: Center(
                child: Text("Assign Class",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,color: Colors.white)),
              ),)

            ),
            ),

            )

          ],
        ),)

     ] ),)


    );
  }

    void GetStudent() async {
      EasyLoading.show(status: 'loading...');
      var response = await http.get(Uri.parse(Urls.Get_Individual_Student+id+"?api_key=F3DEA"),
        headers: {"Accept": "application/json"},
      );
      print(response);
      print('Response status: ${response.statusCode}');
      print('Response body: ${response.body}');
      Map<String, dynamic> value = json.decode(response.body);
      if (response.statusCode == 200) {
        EasyLoading.dismiss();
        try {
          EasyLoading.dismiss();
          Map<String, dynamic> value = json.decode(response.body);

          //var success = value['success'];
          //  if (success) {


            setState(() {
               name = value['name'].toString();
               age = value['age'].toString();
               email = value['email'].toString();
             /*this.name=name;
             this.age=age;
             this.email=email;
             print(this.name);*/
            });

          /* }
        else {
          var message = value['message'];
          EasyLoading.showError(message);
        }*/
        } catch (e) {
          e.toString();
          EasyLoading.dismiss();
        }
      }
      else {
        EasyLoading.dismiss();
        var message = value['message'];
        print(message);
        EasyLoading.showError(message);


      }
    }
    void GetClass() async {
      EasyLoading.show(status: 'loading...');
      var response = await http.get(Uri.parse(Urls.Get_ClassRoom),
        headers: {"Accept": "application/json"},
      );
      print(response);
      print('Response status: ${response.statusCode}');
      print('Response body: ${response.body}');
      Map<String, dynamic> value = json.decode(response.body);
      if (response.statusCode == 200) {
        EasyLoading.dismiss();
        try {
          EasyLoading.dismiss();
          Map<String, dynamic> value = json.decode(response.body);

          //var success = value['success'];
          //  if (success) {
          var subjectArray = value['classrooms'];
          for (int i = 0; i < subjectArray.length; i++) {
            classlist.add(ClassRoomModel(subjectArray[i]['id'].toString(),
              subjectArray[i]['layout'].toString(),
              subjectArray[i]['name'].toString(),
              subjectArray[i]['size'].toString(),
            ));

            setState(() {
              print(classlist.length.toString());
              //GetClass();
            });
          }
          /* }
        else {
          var message = value['message'];
          EasyLoading.showError(message);
        }*/
        } catch (e) {
          e.toString();
          EasyLoading.dismiss();
        }
      }
      else {
        EasyLoading.dismiss();
        var message = value['message'];
        print(message);
        EasyLoading.showError(message);


      }
    }
    Widget setupAlertDiaload() {
      return Container(
        height: 300.0, // Change as per your requirement
        width: 300.0, // Change as per your requirement
        child: ListView.builder(
          shrinkWrap: true,
          itemCount: classlist.length,
          itemBuilder: (BuildContext context, int index) {
            return GestureDetector(
              onTap: (){



                setState(() {
                  Selected_class_id=classlist[index].class_id;
                  GetClassDetail(Selected_class_id);
                });

              },
              child:ListTile(
                title: Text(classlist[index].class_name),
              ),
            );
          },
        ),
      );
    }
    void PostClass(String student_id,String Selected_sub_id) async {
      EasyLoading.show(status: 'loading...');
      var response = await http.post(Uri.parse(Urls.Get_registration+"?api_key=F3DEA"),
          headers: {"Accept": "application/json"},
          body: {
            "student" : student_id,
            "subject" : Selected_sub_id

          });
      print(response);
      print('Response status: ${response.statusCode}');
      print('Response body: ${response.body}');
      Map<String, dynamic> value = json.decode(response.body);
      if (response.statusCode == 200) {
        EasyLoading.dismiss();
        try {
          EasyLoading.dismiss();
          Map<String, dynamic> value = json.decode(response.body);

          GetStudent();

        } catch (e) {
          e.toString();
          EasyLoading.dismiss();
        }
      } else {
        EasyLoading.dismiss();
        var message = value['message'];
        print(message);
        EasyLoading.showError(message);

      }
    }

    void GetClassDetail(String class_id) async {
      EasyLoading.show(status: 'loading...');
      var response = await http.get(Uri.parse(Urls.Get_Individual_Classroom+class_id+"?api_key=F3DEA"),
        headers: {"Accept": "application/json"},
      );
      print(response);

      print('Response status: ${response.statusCode}');
      print('Response body: ${response.body}');
      Map<String, dynamic> value = json.decode(response.body);
      if (response.statusCode == 200) {
        EasyLoading.dismiss();
        try {
          EasyLoading.dismiss();
          Map<String, dynamic> value = json.decode(response.body);

          //var success = value['success'];
          //  if (success) {


          setState(() {
            Navigator.pop(context);
             Selected_sub_id=value['subject'].toString();
            if(Selected_sub_id==""){
              showAlertDialog(context);

            }
            else{
              PostClass(id,Selected_sub_id);
            }
          });

          /* }
        else {
          var message = value['message'];
          EasyLoading.showError(message);
        }*/
        } catch (e) {
          e.toString();
          EasyLoading.dismiss();
        }
      }
      else {
        EasyLoading.dismiss();
        var message = value['message'];
        print(message);
        EasyLoading.showError(message);


      }
    }
    showAlertDialog(BuildContext context) {

      // set up the button
      Widget okButton = TextButton(
        child: Text("OK"),
        onPressed: () {

          Navigator.pop(context);
        },
      );

      // set up the AlertDialog
      AlertDialog alert = AlertDialog(
        title: Text("Alert!!!"),
        content: Text("Please Assign a subject to the class"),
        actions: [
          okButton,
        ],
      );

      // show the dialog
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return alert;
        },
      );
    }

  }