class Urls{

static const BaseUrl= "https://hamon-interviewapi.herokuapp.com/";
static const Get_Students= BaseUrl+"students/?api_key=F3DEA";
static const Get_Subjects=BaseUrl+"subjects/?api_key=F3DEA";
static const Get_ClassRoom=BaseUrl+"classrooms/?api_key=F3DEA";
static const Get_Individual_Student=BaseUrl+"students/";
static const Get_Individual_Subject=BaseUrl+"subjects/";
static const Get_Individual_Classroom=BaseUrl+"classrooms/";
static const Get_registration=BaseUrl+"registration/";
}