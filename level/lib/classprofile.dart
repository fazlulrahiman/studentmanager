import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/http.dart' as http;

import 'ModelClass/SubjectModel.dart';
import 'Utils/Urls.dart';


class ClassProfile extends StatefulWidget{



  String class_id="";
  ClassProfile(this.class_id);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ClassPRstate(this.class_id);
  }

}
class ClassPRstate extends State<ClassProfile>{

  List<SubjectModel>subjectslist=[];
  String Selected_subject_id="";
  String class_id = "";
  String layout="";
  String class_name="";
  String size="";
  String subject="";
  String subject_name="";
  ClassPRstate(this.class_id);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    GetClass();
    GetSubjects();
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        body: SafeArea(
          child:Column(

              children:[
                Expanded(flex: 0,
                    child:Container(height:60,color: Colors.blueGrey,
                      child: Row(
                        children: [
                          Padding(padding: EdgeInsets.only(left: 5),
                              child:GestureDetector(
                                onTap: (){
                                  Navigator.pop(context, true);

                                },
                                child: Icon(Icons.arrow_back,color: Colors.white,),
                              )
                          ),
                          Padding(padding: EdgeInsets.only(left: 5),
                            child: Text("Classroom Profile",style: TextStyle(fontSize:18,fontWeight: FontWeight.bold,color: Colors.white)),

                          )
                        ],
                      ),

                    )
                ),
                Expanded(flex: 1,
                  child: Column(mainAxisAlignment: MainAxisAlignment.center,mainAxisSize: MainAxisSize.max,
                    children: [


                      Expanded(flex:0,

                        child: Padding(padding: EdgeInsets.only(top: 30),
                            child:Center(
                              child: Image.asset("assets/images/class.jpg",height:100,),
                            )
                        ),
                      ),
                      Expanded(flex: 0,
                        child: Column(mainAxisSize: MainAxisSize.max,mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Padding(padding: EdgeInsets.only(top: 20),
                              child: Text(class_name,style: TextStyle(fontSize:25,fontWeight: FontWeight.bold,color: Colors.black)),
                            ),
                            Padding(padding: EdgeInsets.only(),
                              child: Text(layout,style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,color: Colors.black)),
                            ),
                            Padding(padding: EdgeInsets.only(),
                              child: Text(size,style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,color: Colors.black)),
                            ),
                            Padding(padding: EdgeInsets.only(),
                              child: Text("subject: "+subject_name,style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,color: Colors.black)),
                            )
                          ],
                        ),

                      ),
                      Expanded(flex: 0,
                        child: Padding(padding: EdgeInsets.only(top: 50),
                          child:GestureDetector(
                            onTap: (){
                              showDialog(
                                  context:context,
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      title: Text('Subject List'),
                                      content: setupAlertDiaload(),
                                    );
                                  });

                            },
                          child: Container(height:50,width:150,decoration: BoxDecoration(borderRadius: BorderRadius.circular(20),color: Colors.blueGrey),
                            child: Center(
                              child: Text("Assign Subject",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,color: Colors.white)),
                            ),

                          ),)
                        ),

                      )

                    ],
                  ),)

              ] ),)


    );
  }
  void GetClass() async {
    EasyLoading.show(status: 'loading...');
    var response = await http.get(Uri.parse(Urls.Get_Individual_Classroom+class_id+"?api_key=F3DEA"),
      headers: {"Accept": "application/json"},
    );
    print(response);

    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');
    Map<String, dynamic> value = json.decode(response.body);
    if (response.statusCode == 200) {
      EasyLoading.dismiss();
      try {
        EasyLoading.dismiss();
        Map<String, dynamic> value = json.decode(response.body);

        //var success = value['success'];
        //  if (success) {


        setState(() {
          layout = value['layout'].toString();
          class_name = value['name'].toString();
          size = value['size'].toString();
          subject=value['subject'].toString();
          for(int i=0;i<subjectslist.length;i++){
            if(subject!=""){
              if(subject==subjectslist[i].sub_id){
                subject_name = subjectslist[i].sub_name;
              }
            }
            else{
              subject_name="";
            }
          }


        });

        /* }
        else {
          var message = value['message'];
          EasyLoading.showError(message);
        }*/
      } catch (e) {
        e.toString();
        EasyLoading.dismiss();
      }
    }
    else {
      EasyLoading.dismiss();
      var message = value['message'];
      print(message);
      EasyLoading.showError(message);


    }
  }
  void GetSubjects() async {
    EasyLoading.show(status: 'loading...');
    var response = await http.get(Uri.parse(Urls.Get_Subjects),
      headers: {"Accept": "application/json"},
    );
    print(response);
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');
    Map<String, dynamic> value = json.decode(response.body);
    if (response.statusCode == 200) {
      EasyLoading.dismiss();
      try {
        EasyLoading.dismiss();
        Map<String, dynamic> value = json.decode(response.body);

        //var success = value['success'];
        //  if (success) {
        var subjectArray = value['subjects'];
        for (int i = 0; i < subjectArray.length; i++) {
          subjectslist.add(SubjectModel(subjectArray[i]['credits'].toString(),
            subjectArray[i]['id'].toString(),
            subjectArray[i]['name'].toString(),
            subjectArray[i]['teacher'].toString(),
          ));

          setState(() {
            print(subjectslist.length.toString());
            GetClass();
          });
        }
        /* }
        else {
          var message = value['message'];
          EasyLoading.showError(message);
        }*/
      } catch (e) {
        e.toString();
        EasyLoading.dismiss();
      }
    }
    else {
      EasyLoading.dismiss();
      var message = value['message'];
      print(message);
      EasyLoading.showError(message);


    }
  }
  Widget setupAlertDiaload() {
    return Container(
      height: 300.0, // Change as per your requirement
      width: 300.0, // Change as per your requirement
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: subjectslist.length,
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            onTap: (){
              Navigator.pop(context);
           setState(() {
             Selected_subject_id=subjectslist[index].sub_id;
             PostSubject(Selected_subject_id);
           });

            },
            child:ListTile(
              title: Text(subjectslist[index].sub_name),
            ),
          );
        },
      ),
    );
  }

  void PostSubject(String subject) async {
    EasyLoading.show(status: 'loading...');
    var response = await http.patch(Uri.parse(Urls.Get_Individual_Classroom+class_id+"?api_key=F3DEA"),
        headers: {"Accept": "application/json"},
        body: {
          "subject" : subject,

        });
    print(response);
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');
    Map<String, dynamic> value = json.decode(response.body);
    if (response.statusCode == 200) {
      EasyLoading.dismiss();
      try {
        EasyLoading.dismiss();
        Map<String, dynamic> value = json.decode(response.body);

        GetClass();

      } catch (e) {
        e.toString();
        EasyLoading.dismiss();
      }
    } else {
      EasyLoading.dismiss();
      var message = value['message'];
      print(message);
      EasyLoading.showError(message);


    }
  }

}