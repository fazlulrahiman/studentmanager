import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/http.dart' as http;
import 'package:level/classprofile.dart';
import 'package:page_transition/page_transition.dart';
import 'ModelClass/ClassRoomModel.dart';
import 'Utils/Urls.dart';

class ClassRoom extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return Classroomstate();
  }

}
class Classroomstate extends State<ClassRoom>{
  List<ClassRoomModel>classroomlist=[];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    GetClassrooms();
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        body: SafeArea(
          child:Column(
            children: [
              Expanded(flex: 0,
                child: Container(
                  height: 60,width: double.infinity,color: Colors.blueGrey,
                  child: Row(
                    children: [
                      Padding(padding: EdgeInsets.only(left: 10),
                          child:GestureDetector(
                            onTap: (){

                              Navigator.pop(context, true);
                            },
                            child: Icon(Icons.arrow_back,color: Colors.white,),

                          )

                      ),
                      Padding(padding: EdgeInsets.only(left:5),
                        child: Text("Class Room",style: TextStyle(fontSize:18,fontWeight: FontWeight.bold,color: Colors.white),),

                      )
                    ],
                  ),

                ),
              ),
              Expanded(flex: 1,
                child: Container(
                    child: Center(
                        child:ListView.builder(
                          itemCount: classroomlist.length,
                          shrinkWrap: true,
                          scrollDirection: Axis.vertical,
                          itemBuilder: (BuildContext context, int index) {
                            return Padding(padding: EdgeInsets.all(10),
                                child: GestureDetector(
                                    onTap: () {
                                      Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: ClassProfile(classroomlist[index].class_id)));
                                    },
                                    child: Classlist(index)));
                          },
                        )

                    )
                ),
              )
            ],


          ),)

    );
  }
  Widget Classlist(int index){
    return Card(elevation: 10,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),),

      child: Container(height: 100,
        child: Row(
          children: [
            Expanded(flex: 0,
              child: Container(height: 80,
                  child:Padding(padding: EdgeInsets.only(left: 5,right: 5,top: 5,bottom: 5),
                    child: Image.asset("assets/images/class.jpg",),
                  ) ),
            ),
            Expanded(flex: 1,
              child: Column(mainAxisSize: MainAxisSize.max,mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(flex: 0,
                      child:
                      Align(alignment:Alignment.centerLeft,
                        child:
                        Padding(padding: EdgeInsets.only(left:5,right: 5,top:5,bottom:5),
                          child: Text(classroomlist[index].class_name,style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold,color: Colors.black)),
                        ), ) ),
                  Expanded(flex:0,
                      child:Align(alignment:Alignment.centerLeft,
                        child:
                        Padding(padding: EdgeInsets.only(left:5,right: 5,top: 5,bottom: 5),
                          child: Text(classroomlist[index].layout,style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,color: Colors.black)),
                        ),)  ),

                ],
              ),
            ),


          ],
        ),



      ),

    );


  }
  void GetClassrooms() async {
    EasyLoading.show(status: 'loading...');
    var response = await http.get(Uri.parse(Urls.Get_ClassRoom),
      headers: {"Accept": "application/json"},
    );
    print(response);
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');
    Map<String, dynamic> value = json.decode(response.body);
    if (response.statusCode == 200) {
      EasyLoading.dismiss();
      try {
        EasyLoading.dismiss();
        Map<String, dynamic> value = json.decode(response.body);

        //var success = value['success'];
        //  if (success) {
        var classroomArray = value['classrooms'];
        for (int i = 0; i < classroomArray.length; i++) {
          classroomlist.add(ClassRoomModel(classroomArray[i]['id'].toString(),
              classroomArray[i]['layout'].toString(),
              classroomArray[i]['name'].toString(),
              classroomArray[i]['size'].toString()));

          setState(() {
            print(classroomlist.length.toString());
          });
        }
        /* }
        else {
          var message = value['message'];
          EasyLoading.showError(message);
        }*/
      } catch (e) {
        e.toString();
        EasyLoading.dismiss();
      }
    }
    else {
      EasyLoading.dismiss();
      var message = value['message'];
      print(message);
      EasyLoading.showError(message);


    }
  }

}