import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:level/classroom.dart';
import 'package:level/studentdetails.dart';
import 'package:level/subjectdetails.dart';
import 'package:page_transition/page_transition.dart';

class Dashboard extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return Dashboardstate();
  }

}
class Dashboardstate extends State<Dashboard>{

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: SafeArea(
        child: Column(mainAxisSize: MainAxisSize.max,
          children: [
            Expanded(flex: 0,
              child: Column(mainAxisSize: MainAxisSize.max,
                children: [
                  Expanded(flex:0,
                  child:
                  Padding(padding: EdgeInsets.only(top: 20),
                  child:Align(alignment: Alignment.center,
                  child: Image.asset("assets/images/school.png",height: 100,),
                     )),
      ),
                    Padding(padding: EdgeInsets.only(top: 10),
                    child: Text("HILITE VALLEY",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
                    ),
                  Padding(padding: EdgeInsets.only(top: 10),
                    child:Text("English Medium School",style: TextStyle(fontStyle: FontStyle.italic,fontSize: 15),)
                  ),
                  Padding(padding: EdgeInsets.only(top: 10),
                  child:Text("Banglore-PB-9078",style: TextStyle(fontStyle: FontStyle.italic,fontSize: 15))
                  )
                ],
              
              ),

            ),
            Expanded(flex:0,
            child: Padding(padding: EdgeInsets.only(top: 30,left:30,right: 30),
            child: stud(),
            ),

            )
          ],
        ),
      ),

    );
  }
  Widget stud(){
    return Column(mainAxisSize: MainAxisSize.max,
      children: [
        Align(alignment: Alignment.topLeft,
          child:GestureDetector(
            onTap: (){
              Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: Student_list()));

            },
          child:Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
            elevation: 10,
        child:
        Container(height:60,width:double.infinity,
          child: Row(
            children: [
              Padding(padding: EdgeInsets.only(left: 10),
              child: Icon(Icons.person,size: 30,),
              ),
              Padding(padding: EdgeInsets.only(left: 5),
              child: Text("Students"),
              )
            ],
          ),
        )),),),
        Padding(padding:EdgeInsets.only(top:20),
        child:
        Align(alignment: Alignment.topLeft,
          child:GestureDetector(
            onTap: (){
              Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: Subjectlist()));

            },
          child:Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              elevation: 10,
              child:
              Container(height:60,width:double.infinity,
                child: Row(
                  children: [
                    Padding(padding: EdgeInsets.only(left: 10),
                      child: Icon(Icons.subject,size: 30,),
                    ),
                    Padding(padding: EdgeInsets.only(left: 5),
                      child: Text("Subject"),
                    )
                  ],
                ),
              ) ),),) ),
        Padding(padding:EdgeInsets.only(top: 20),
        child:
        Align(alignment: Alignment.topLeft,
          child:GestureDetector(
            onTap: (){
              Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: ClassRoom()));

            },
          child:Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              elevation: 10,
              child:
              Container(height:60,width:double.infinity,
                child: Row(
                  children: [
                    Padding(padding: EdgeInsets.only(left: 10),
                      child: Icon(Icons.school,size: 30,),
                    ),
                    Padding(padding: EdgeInsets.only(left: 5),
                      child: Text("Class"),
                    )
                  ],
                ),
              )),),) ),
        Padding(padding: EdgeInsets.only(top: 60),
        child: Row(mainAxisSize: MainAxisSize.max,mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(flex:0,
            child:Padding(padding: EdgeInsets.only(),

              child: Icon(Icons.call),
            ),
            ),
            Padding(padding: EdgeInsets.only(left: 5),
            child: Text("+91 9747770067",style: TextStyle(fontWeight: FontWeight.bold,fontSize:15),),
            )
          ],
        ),
        ),
        Padding(padding: EdgeInsets.only(top:20),
          child: Row(mainAxisSize: MainAxisSize.max,mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(flex:0,
                child:Padding(padding: EdgeInsets.only(),

                  child: Icon(Icons.mail),
                ),
              ),
              Padding(padding: EdgeInsets.only(left: 5),
                child: Text("Hilitevalley@outlook.om",style: TextStyle(fontWeight: FontWeight.bold,fontSize:15),),
              )
            ],
          ),
        ),


] );

  }

}