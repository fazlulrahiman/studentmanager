import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/http.dart' as http;
import 'Utils/Urls.dart';

class SubjectProfile extends StatefulWidget{
  String sub_id = "";
  SubjectProfile(this.sub_id);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return SubjectPRstate(this.sub_id);
  }

}
class SubjectPRstate extends State<SubjectProfile>{
  String sub_id = "";
  String credits="";
  String sub_name="";
  String teacher="";
  SubjectPRstate(this.sub_id);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    GetSubject();
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        body: SafeArea(
          child:Column(

              children:[
                Expanded(flex: 0,
                    child:Container(height:60,color: Colors.blueGrey,
                      child: Row(
                        children: [
                          Padding(padding: EdgeInsets.only(left: 5),
                              child:GestureDetector(
                                onTap: (){
                                  Navigator.pop(context, true);

                                },
                                child: Icon(Icons.arrow_back,color: Colors.white,),
                              )
                          ),
                          Padding(padding: EdgeInsets.only(left: 5),
                            child: Text("Subject Profile",style: TextStyle(fontSize:18,fontWeight: FontWeight.bold,color: Colors.white)),

                          )
                        ],
                      ),

                    )
                ),
                Expanded(flex: 1,
                  child: Column(mainAxisAlignment: MainAxisAlignment.center,mainAxisSize: MainAxisSize.max,
                    children: [


                      Expanded(flex:0,

                        child: Padding(padding: EdgeInsets.only(top: 30),
                            child:Center(
                              child: Image.asset("assets/images/sub.png",height:100,),
                            )
                        ),
                      ),
                      Expanded(flex: 0,
                        child: Column(mainAxisSize: MainAxisSize.max,mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Padding(padding: EdgeInsets.only(top: 20),
                              child: Text(sub_name,style: TextStyle(fontSize:25,fontWeight: FontWeight.bold,color: Colors.black)),
                            ),
                            Padding(padding: EdgeInsets.only(),
                              child: Text("crdits: "+credits,style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,color: Colors.black)),
                            ),
                            Padding(padding: EdgeInsets.only(),
                              child: Text("Teacher: "+teacher,style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,color: Colors.black)),
                            )
                          ],
                        ),

                      ),
/*
                      Expanded(flex: 0,
                        child: Padding(padding: EdgeInsets.only(top: 50),
                          child: Container(height:50,width:150,decoration: BoxDecoration(borderRadius: BorderRadius.circular(20),color: Colors.blueGrey),
                            child: Center(
                              child: Text("Assign",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,color: Colors.white)),
                            ),

                          ),
                        ),

                      )
*/

                    ],
                  ),)

              ] ),)


    );
  }
  void GetSubject() async {
    EasyLoading.show(status: 'loading...');
    var response = await http.get(Uri.parse(Urls.Get_Individual_Subject+sub_id+"?api_key=F3DEA"),
      headers: {"Accept": "application/json"},
    );
    print(response);

    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');
    Map<String, dynamic> value = json.decode(response.body);
    if (response.statusCode == 200) {
      EasyLoading.dismiss();
      try {
        EasyLoading.dismiss();
        Map<String, dynamic> value = json.decode(response.body);

        //var success = value['success'];
        //  if (success) {


        setState(() {
          print(credits);
          credits = value['credits'].toString();
          sub_name = value['name'].toString();
          teacher = value['teacher'].toString();

        });

        /* }
        else {
          var message = value['message'];
          EasyLoading.showError(message);
        }*/
      } catch (e) {
        e.toString();
        EasyLoading.dismiss();
      }
    }
    else {
      EasyLoading.dismiss();
      var message = value['message'];
      print(message);
      EasyLoading.showError(message);


    }
  }

}